IDWard - Frontend
===============

This repository contains code for idWard

Table of Contents
---------------

- [Getting Started](#getting-started)
- [Project Structure](#project-structure)
- [Run locally](#run-locally)
- [Commits](#commits)
- [Deploy](#deploy)
- [License](#license)



Getting Started
---------------


### Requirements

- Node.js [link](https://nodejs.org/en/)
- npm (With Node)
- git version control system [link](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)


### Clone repository & install

Download the code:

```bash
# Get the latest snapshot
git clone https://javison@bitbucket.org/consent_plugin.git

# Change directory
cd consent-plugin

# Install NPM dependencies
npm install
```

Project Structure
---------------

| Name                               | Description                                                  |
| ---------------------------------- | ------------------------------------------------------------ |
| **src/app**                        | Root code folder                                             |
| **src/assets**                     | Images and other static files                                |
| **src/environments**               | Environment configuration files: local, pre, prod            |
| _variables.scss                    | scsss project globar vars                                    |
| custom-component-themes.scss       | Custom angular material theme styles                         |
| styles.scss                        | Project common styles                                        |
| theme.scss                         | Custom angular material theme styles                         |
| **src/app/api**                    | Auto generated api integration                               |
| **src/app/auth**                   |                                |
| **src/app/core**                   |                                |
| **src/app/material**               |                                |
| **src/app/pages**                  | Main project pages                                           |
| **src/app/shared**                 | Shared code among the project                                |
| **src/app/shared/components**      | Reusable page sections such as footer, navbar, sidebar..     |
| **src/app/shared/guards**          | Restrict access to certain parts of website.                 |
| **src/app/shared/interfaces**      | Data model descriptions                                      |
| **src/app/shared/models**          | Data classes to implement                                    |
| **src/app/shared/resolvers**       | Return promiso or observer with condition before load a path |
| **src/app/shared/services**        | Main business logic. Class with well-defined purpose.        |
| **src/app/shared/utils**           | Static methods for different purpose.                        |
| **dist**/                          | Public folder with build (tsc) components to deploy          |
| **coverage**/                      | Testing coverage html reports                                |
| **documentation**/                 | Project technical documentation                              |
| **node_modules**/                  | npm dependency packages                                      |
| .gitignore                         | Git ignore config.                                           |
| .prettier                          | Makes code prettier. Config vscode to auto execute on file save |
| .nvmrc                          | Node Version Manager  |
| karma.conf.js                      | Check code styles before commit                              |
| package.json                       | NPM dependencies.                                            |
| angular.json                       | Angular configuration file                                   |
| tsconfig.json                      | Typescript configuration file                                |
| tslint.json                        | TypeScript analysis tool that checks code for readability    |



Run locally
---------------

```bash
# If you are running backend server
$ npm run start:local 

# If you are NOT running backend server locally, 
# you can connect to cloud environment
$ npm run start:pre
```



Commits
---------------
# Conventional Commits

https://www.conventionalcommits.org/

- build: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- ci: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
- docs: Documentation only changes
- feat: A new feature
- fix: A bug fix
- perf: A code change that improves performance
- refactor: A code change that neither fixes a bug nor adds a feature
- style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- test: Adding missing tests or correcting existing tests


Deploy
---------------





Coding recommendations
---------------

### Integrated development environment 
App developed with Visual studio code [link](https://code.visualstudio.com/).
Plugins used for Visual Studio Code:

- Angular Language Service
- angular2-inline
- Bracket Pair Colorizer 2
- GitLens
- jshint
- Prettier
- TypeScript Hero
- TypeScript Importer
- vscode-icons


### Standarts
- Write comments and documentation
- Write readable yet efficient code
- Write tests
- SOLID principles [more info](https://en.wikipedia.org/wiki/SOLID)
- Follow lint recommendations


License
---------------

Copyright (c) 2021