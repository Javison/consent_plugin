import { IEnvironment } from '@app/core/models/ienvironment';

import { version } from '../../package.json';

export const environment: IEnvironment = {
  production: false,
  apiUrl: 'https://TODO-FILL.ey.r.appspot.com/',
  version,
  features: {},
  iconsPath: '/assets/icons',
  googleAnalyticsMeasurementId: 'G-TODO_DEMO'
};
