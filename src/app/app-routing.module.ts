import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Page404Component } from './pages/public/page404/page404.component';

const routes: Routes = [
  {
    path: 'pages',
    pathMatch: 'prefix',
    loadChildren: () =>
      import('./pages/protected/internal-pages.module').then(
        (m) => m.InternalPagesModule,
      ),
  },
  {
    path: 'p',
    pathMatch: 'full',
    loadChildren: () =>
      import('./pages/public/public-pages.module').then(
        (m) => m.PublicPagesModule,
      ),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'p',
  },
  {
    path: '404',
    pathMatch: 'full',
    component: Page404Component,
  },
  {
    path: '**',
    redirectTo: '404',
  },
];

@NgModule({
  declarations: [Page404Component],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
