import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NGXLogger } from 'ngx-logger';

import { EnvironmentService, IconService } from './core/services';
import { ConsentDialogComponent } from './pages/public/components/consent-dialog/consent-dialog.component';
import { AnalyticsService } from './shared/services/analytics.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'consent-plugin';

  version = '0.0.0';

  constructor(
    private log: NGXLogger,
    private matDialog: MatDialog,
    private matSnackBar: MatSnackBar,
    private analyticsService: AnalyticsService,
    private environment: EnvironmentService,
    private iconService: IconService
  ) {
    this.version = environment.version;
  }

  ngOnInit(): void {
    this.iconService.registerIcons();
  }


  onClickPrivacySettings(): void {
    this.log.debug('showConsentDialog()');

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.matDialog.open(ConsentDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((privacyConsent: boolean) => {
      this.log.debug('dialogRef.afterClosed - privacyConsent:', privacyConsent);
      if (privacyConsent) {
        this.log.debug('dialogRef.afterClosed - privacy consent');
        this.analyticsService.startTracking();
        this.analyticsService.trackEvent('user_privacy', 'button_click', 'accepted');

        this.matSnackBar.open('Privacy consent accepted.', 'Ok', {
          duration: 5000,
        });

      } else {
        this.analyticsService.stopTracking();

        this.matSnackBar.open('Privacy consent denied. You can always change your configuration later.', 'Ok', {
          duration: 5000,
        });
      }
    });
  }

}
