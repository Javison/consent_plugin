import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { NGXLogger } from 'ngx-logger';
import { ReplaySubject } from 'rxjs';


declare const ga: any;

export interface AnalyticsEvent {
  type: 'PAGEVIEW' | 'EVENT';
  category?: string;
  action?: string;
  label: string;
}


@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  eventsQueue$ = new ReplaySubject<AnalyticsEvent>();

  // TODO READ
  // https://developers.google.com/gtagjs/devguide/consent
  // https://blexin.com/en/blog-en/google-analytics/

  constructor(
    private log: NGXLogger
  ) {}

  /**
   * Start tracking google analytics
   */
  public startTracking(): void {
    this.log.debug('startTracking');
    ga('create', environment.googleAnalyticsMeasurementId, 'auto');
    // ga('config', environment.GoogleAnalyticsMeasurementId);
    this.subscribeToEvents();
  }

  private subscribeToEvents(): void {
    this.log.debug('subscribeToEvents');
    this.eventsQueue$.subscribe((e: AnalyticsEvent) => {
      if (e.type === 'PAGEVIEW') {
        ga('send', {
            hitType: 'pageview',
            page: e.label,
          });
      } else if (e.type === 'EVENT') {
        ga('send', {
            hitType: 'event',
            eventCategory: e.category,
            eventAction: e.action,
            eventLabel: e.label,
          });
      }
    });
  }

  /**
   * Stop tracking google analytics
   */
  public stopTracking(): void {
    ga('config', 'null');
  }

  public trackVirtualPageView(name: string): void {
    this.log.debug('trackVirtualPageView');
    this.eventsQueue$.next({type: 'PAGEVIEW', label: name});
  }

  public trackEvent(category: string, action: string, label: string): void {
    this.log.debug('trackEvent');
    this.eventsQueue$.next({type: 'EVENT', category, action, label});
  }
}
