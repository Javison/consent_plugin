import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../material/material.module';

const SHARED_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, RouterModule];
const ANGULAR_MODULES = [MaterialModule];
const EXPORT_COMPONENTS: never[] = [];

@NgModule({
  declarations: [],
  imports: [...ANGULAR_MODULES, ...SHARED_MODULES],
  exports: [...ANGULAR_MODULES, ...SHARED_MODULES, ...EXPORT_COMPONENTS],
})
export class SharedModule { }
