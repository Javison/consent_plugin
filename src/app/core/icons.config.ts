/**
 * Custom icons for material icons system.
 *
 * To add a new custom icon:
 *
 * 1. Create de icon file in SVG format
 * 2. Copy icon file to /assets/icons directory.
 *    (please, be sure to use only lowercase letters, numbers and hyphen
 *    to avoid any problem with URLs)
 * 3. Add an object to this list with:
 *    - `name`: the name or code
 */

export const APP_ICONS = [
  { name: 'logo', file: 'logo' },
  { name: 'circle-navbar', file: 'circle-navbar' },
  { name: 'my-space', file: 'my-space' },
  { name: 'home', file: 'home' },
  { name: 'search', file: 'search' },
  { name: 'settings', file: 'settings' },
];
