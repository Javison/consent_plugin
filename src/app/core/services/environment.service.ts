import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';

import { IEnvironment } from '../models/ienvironment';

@Injectable({
  providedIn: 'root',
})
export class EnvironmentService implements IEnvironment {
  get apiUrl(): string {
    return environment.apiUrl;
  }

  get features(): string {
    return environment.features;
  }

  get iconsPath(): string {
    return environment.iconsPath;
  }

  get production(): boolean {
    return environment.production;
  }

  get version(): string {
    return environment.version;
  }

  get googleAnalyticsMeasurementId(): string {
    return environment.googleAnalyticsMeasurementId;
  }

  constructor() {}
}
