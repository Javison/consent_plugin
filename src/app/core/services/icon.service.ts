import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '@environments/environment';

import { APP_ICONS } from '../icons.config';

@Injectable({
  providedIn: 'root',
})
export class IconService {
  readonly APP_NAME: string = 'app';

  constructor(private iconReg: MatIconRegistry, private sanitizer: DomSanitizer) {}

  public registerIcons(): void {
    this.initIcons();
  }

  private initIcons(): void {
    for (const icon of APP_ICONS) {
      this.iconReg.addSvgIconInNamespace(
        this.APP_NAME,
        icon.name,
        this.sanitizer.bypassSecurityTrustResourceUrl(
          `${environment.iconsPath}/${icon.file}.svg`,
        ),
      );
    }
  }
}
