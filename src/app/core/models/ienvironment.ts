export interface IEnvironment {
  apiUrl: string;
  iconsPath: string;
  features: any;
  production: boolean;
  version: string;
  googleAnalyticsMeasurementId: string;
}
