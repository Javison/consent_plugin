import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { MaterialModule } from '../material/material.module';
import { throwIfAlreadyLoaded } from './module-import-guard';
import { EnvironmentService, IconService } from './services';

export const CORE_PROVIDERS = [EnvironmentService, IconService];

/**
 * Core module
 */
@NgModule({
  declarations: [],
  imports: [CommonModule, MaterialModule],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [...CORE_PROVIDERS],
    };
  }
}
