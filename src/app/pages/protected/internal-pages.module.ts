import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';

import { InternalPagesRoutingModule } from './internal-pages-routing.module';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    InternalPagesRoutingModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
  ],
})
export class InternalPagesModule { }
