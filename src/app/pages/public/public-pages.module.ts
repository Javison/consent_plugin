import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';

import { ConsentDialogComponent } from './components/consent-dialog/consent-dialog.component';
import { HomeComponent } from './home/home.component';
import { LandingComponent } from './landing/landing.component';
import { PublicPagesRoutingModule } from './public-pages-routing.module';



@NgModule({
  declarations: [
    LandingComponent,
    HomeComponent,
    ConsentDialogComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    PublicPagesRoutingModule,
  ],
  exports: [
    SharedModule,
  ],
})
export class PublicPagesModule { }
