import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConsentDialogComponent } from '@app/pages/public/components/consent-dialog/consent-dialog.component';
import { AnalyticsService } from '@app/shared/services/analytics.service';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private log: NGXLogger,
    private matDialog: MatDialog,
    private matSnackBar: MatSnackBar,
    private analyticsService: AnalyticsService,
  ) { }

  ngOnInit(): void {
    this.log.debug('OnInit');
    this.showConsentDialog();
  }

  showConsentDialog(): void {
    this.log.debug('showConsentDialog()');

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.matDialog.open(ConsentDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((privacyConsent: boolean) => {
      this.log.debug('dialogRef.afterClosed - privacyConsent:', privacyConsent);
      if (privacyConsent) {
        this.log.debug('dialogRef.afterClosed - privacy consent');
        this.analyticsService.startTracking();

        this.matSnackBar.open('Privacy consent accepted.', 'Ok', {
          duration: 5000,
        });
      } else {
        this.matSnackBar.open('Privacy consent denied. You can change your configuration in button option.', 'Ok', {
          duration: 5000,
        });
      }
    });
  }

}
