import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-consent-dialog',
  templateUrl: './consent-dialog.component.html',
  styleUrls: ['./consent-dialog.component.scss']
})
export class ConsentDialogComponent implements OnInit {

  isAnyConsent = true;

  isActivityConsent = false;
  isLocationsConsent = false;
  isStreamingConsent = false;

  constructor(
    private log: NGXLogger,
    public dialogRef: MatDialogRef<ConsentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    // TODO Load from localStorage if the user already accepted
    this.isActivityConsent = Boolean(localStorage.getItem('privacy_activity') === 'true');
    this.isLocationsConsent = Boolean(localStorage.getItem('privacy_locations') === 'true');
    this.isStreamingConsent = Boolean(localStorage.getItem('privacy_streaming') === 'true');
  }

  onClickConsent(): void {
    this.log.debug('onClickConsent', this.isAnyConsent);
    localStorage.setItem('privacy_activity', String(this.isActivityConsent));
    localStorage.setItem('privacy_locations', String(this.isLocationsConsent));
    localStorage.setItem('privacy_streaming', String(this.isStreamingConsent));
    this.dialogRef.close(true);
  }

  onClickDissent(): void {
    this.log.debug('onClickDissent', this.isAnyConsent);
    localStorage.setItem('privacy_activity', 'false');
    localStorage.setItem('privacy_locations', 'false');
    localStorage.setItem('privacy_streaming', 'false');
    this.dialogRef.close(false);
  }

  onToggleActivity(event: any): void {
    this.log.debug('onToggleActivity - event: ', event);
    this.isActivityConsent = event.checked;
  }

  onToggleLocations(event: any): void {
    this.log.debug('onToggleLocations - event: ', event);
    this.isLocationsConsent = event.checked;
  }

  onToggleStreaming(event: any): void {
    this.log.debug('onToggleStreaming - event: ', event);
    this.isStreamingConsent = event.checked;
  }

}
